From 716db242314b710b881c073d290b6d1ad8670d36 Mon Sep 17 00:00:00 2001
From: Vendula Poncova <vponcova@redhat.com>
Date: Mon, 6 Jul 2020 14:19:46 +0200
Subject: [PATCH] Add support for generating a summary of the NTP servers

Call the functions get_ntp_server_summary and get_ntp_servers_summary to
generate a string with a summary of the specified NTP servers and their
states.
---
 pyanaconda/ntp.py | 35 +++++++++++++++++++++++++++++++++++
 1 file changed, 35 insertions(+)

diff --git a/pyanaconda/ntp.py b/pyanaconda/ntp.py
index 637d31f63e..eed4b34307 100644
--- a/pyanaconda/ntp.py
+++ b/pyanaconda/ntp.py
@@ -60,6 +60,41 @@ class NTPconfigError(Exception):
     pass
 
 
+def get_ntp_server_summary(server, states):
+    """Generate a summary of an NTP server and its status.
+
+    :param server: an NTP server
+    :type server: an instance of TimeSourceData
+    :param states: a cache of NTP server states
+    :type states: an instance of NTPServerStatusCache
+    :return: a string with a summary
+    """
+    return "{} ({})".format(
+        server.hostname,
+        states.get_status_description(server)
+    )
+
+
+def get_ntp_servers_summary(servers, states):
+    """Generate a summary of NTP servers and their states.
+
+    :param servers: a list of NTP servers
+    :type servers: a list of TimeSourceData
+    :param states: a cache of NTP server states
+    :type states: an instance of NTPServerStatusCache
+    :return: a string with a summary
+    """
+    summary = _("NTP servers:")
+
+    for server in servers:
+        summary += "\n" + get_ntp_server_summary(server, states)
+
+    if not servers:
+        summary += " " + _("not configured")
+
+    return summary
+
+
 def ntp_server_working(server_hostname):
     """Tries to do an NTP request to the server (timeout may take some time).
 
--
2.23.0
